<?php

namespace Drupal\ai_interpolator_deepgram\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator_deepgram\DeepgramText;

/**
 * The rules for a string long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_deepgram_string_long",
 *   title = @Translation("Deepgram Transcribe"),
 *   field_rule = "string_long",
 * )
 */
class DeepgramStringLong extends DeepgramText {

  /**
   * {@inheritDoc}
   */
  public $title = 'Deepgram Transcribe';

}
