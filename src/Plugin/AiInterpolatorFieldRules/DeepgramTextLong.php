<?php

namespace Drupal\ai_interpolator_deepgram\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator_deepgram\DeepgramText;

/**
 * The rules for a text long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_deepgram_text_long",
 *   title = @Translation("Deepgram Transcribe"),
 *   field_rule = "text_long",
 * )
 */
class DeepgramTextLong extends DeepgramText {

  /**
   * {@inheritDoc}
   */
  public $title = 'Deepgram Transcribe';

}
