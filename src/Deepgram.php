<?php

namespace Drupal\ai_interpolator_deepgram;

use Drupal\ai_interpolator_deepgram\Form\DeepgramConfigForm;
use Drupal\Core\Config\ConfigFactory;
use Drupal\file\Entity\File;
use GuzzleHttp\Client;

/**
 * Deepgram API creator.
 */
class Deepgram {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * API Key.
   */
  private string $apiKey;

  /**
   * The base path.
   */
  private string $basePath = 'https://api.deepgram.com/v1/';

  /**
   * Constructs a new Deepgram object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(Client $client, ConfigFactory $configFactory) {
    $this->client = $client;
    $this->apiKey = $configFactory->get(DeepgramConfigForm::CONFIG_NAME)->get('api_key') ?? '';
  }

  /**
   * Generate a transcription.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file to transcribe.
   * @param array $config
   *   Other configs.
   *
   * @return string
   *   The transcription.
   */
  public function transcribe(File $file, $config = []) {
    if (!$this->apiKey) {
      return [];
    }
    $query['model'] = $config['model'] ?? 'general';
    $query['tier'] = $config['tier'] ?? 'base';
    if (!empty($config['language'])) {
      $query['language'] = $config['language'];
    }
    $query['detect_language'] = $config['detect_language'] ?? TRUE;
    $query['punctuate'] = $config['punctuate'] ?? TRUE;
    $query['diarize'] = $config['diarize'] ?? FALSE;
    $query['smart_format'] = $config['smart_format'] ?? FALSE;
    $query['filter_words'] = $config['filter_words'] ?? FALSE;

    $guzzleOptions['headers']['Accept'] = 'application/json';
    return json_decode($this->makeRequest("listen", [], 'POST', file_get_contents($file->getFileUri()), $guzzleOptions)->getContents(), TRUE);
  }

  /**
   * Make Deepgram call.
   *
   * @param string $path
   *   The path.
   * @param array $query_string
   *   The query string.
   * @param string $method
   *   The method.
   * @param string $body
   *   Data to attach if POST/PUT/PATCH.
   * @param array $options
   *   Extra headers.
   *
   * @return string|object
   *   The return response.
   */
  protected function makeRequest($path, array $query_string = [], $method = 'GET', $body = '', array $options = []) {
    // We can wait some.
    $options['connect_timeout'] = 30;
    $options['read_timeout'] = 30;
    // Don't let Guzzle die, just forward body and status.
    //$options['http_errors'] = FALSE;
    // Headers.
    $options['headers']['Authorization'] = 'Token ' . $this->apiKey;

    if ($body) {
      $options['body'] = $body;
    }

    $new_url = $this->basePath . $path;
    $new_url .= count($query_string) ? '?' . http_build_query($query_string) : '';

    $res = $this->client->request($method, $new_url, $options);

    return $res->getBody();
  }

}
